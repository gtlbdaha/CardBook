<!ENTITY generalTabLabel "Allgemein">
<!ENTITY CardBookTabLabel "CardBook Tab">
<!ENTITY editionTabLabel "Edition">
<!ENTITY IMPPsTabLabel "IM Adressen">
<!ENTITY customFieldsTabLabel "Benutzerdefinierte Felder">
<!ENTITY listsTabLabel "Listen">
<!ENTITY syncTabLabel "Synchronisation">
<!ENTITY emailTabLabel "Email">
<!ENTITY birthdaylistTabLabel "Geburtstage">
<!ENTITY synclistTabLabel "Geburtstage-Erinnerungen">

<!ENTITY integrationGroupboxLabel "Integration in Thunderbird">
<!ENTITY autocompletionLabel "Autovervollständigung der Email-Adressen">
<!ENTITY autocompletionAccesskey "A">
<!ENTITY autocompletionTooltip "Erweitert die standardmäßige Autovervollständigung von Kontakten in Adressbüchern von Thunderbird um eine Autovervollständigung von Kontakten in Adressbüchern von CardBook.">
<!ENTITY autocompleteSortByPopularityLabel "Sortiere Email-Adressen nach Popularität">
<!ENTITY autocompleteSortByPopularityAccesskey "S">
<!ENTITY autocompleteProposeConcatEmailsLabel "Verknüpfte E-Mails pro Kontakt vorschlagen">
<!ENTITY autocompleteProposeConcatEmailsAccesskey "V">
<!ENTITY autocompleteShowAddressbookLabel "Zeige den Namen des Adressbuchs">
<!ENTITY autocompleteShowAddressbookAccesskey "Z">
<!ENTITY autocompleteWithColorLabel "Benutze die Farbe des Adressbuchs">
<!ENTITY autocompleteWithColorAccesskey "F">
<!ENTITY autocompleteRestrictSearchLabel "Begrenze die durchsuchten Felder">
<!ENTITY autocompleteRestrictSearchAccesskey "P">
<!ENTITY chooseAutocompleteRestrictSearchFieldsButtonLabel "Hinzufügen">
<!ENTITY chooseAutocompleteRestrictSearchFieldsButtonAccesskey "H">
<!ENTITY resetAutocompleteRestrictSearchFieldsButtonLabel "Zurücksetzen auf Standard">
<!ENTITY resetAutocompleteRestrictSearchFieldsButtonAccesskey "Z">
<!ENTITY useColorLabel "Benutze die Farbe des Adressbuchs als:">
<!ENTITY useColorBackgroundLabel "Hintergrundfarbe">
<!ENTITY useColorBackgroundAccesskey "g">
<!ENTITY useColorTextLabel "Textfarbe">
<!ENTITY useColorTextAccesskey "T">
<!ENTITY useColorNothingLabel "Nichts">
<!ENTITY useColorNothingAccesskey "N">
<!ENTITY exclusiveLabel "Einzige Quelle für Kontakte">
<!ENTITY exclusiveAccesskey "E">
<!ENTITY exclusiveTooltip "Falls deaktiviert, ermöglicht diese Einstellung die Zuordnung von Kontakten aus den Standardadressbüchern von Thunderbird für die Gelben Sterne, die Anzeigenamen und die Autovervollständigung.">
<!ENTITY accountsRestrictionsLabel "Beschränkungen für Mail-Konten">
<!ENTITY accountsRestrictionsEnabledLabel "Aktiviert">
<!ENTITY accountsRestrictionsMailNameLabel "Email-Konto">
<!ENTITY accountsRestrictionsABNameLabel "Adressbuch">
<!ENTITY accountsRestrictionsCatNameLabel "Kategorie">
<!ENTITY accountsRestrictionsIncludeNameLabel "Art">
<!ENTITY addRestrictionLabel "Hinzufügen">
<!ENTITY addRestrictionAccesskey "H">
<!ENTITY renameRestrictionLabel "Bearbeiten">
<!ENTITY renameRestrictionAccesskey "B">
<!ENTITY deleteRestrictionLabel "Löschen">
<!ENTITY deleteRestrictionAccesskey "L">
<!ENTITY emailsCollectionLabel "Sammlung ausgehender Email-Adressen">
<!ENTITY emailsCollectionTooltip "Erweitert die standardmäßige Sammlung ausgehender Email-Adressen in Thunderbird. Falls aktiviert, werden Email-Adressen, die nicht in einem Adressbuch von CardBook vorhanden sind, automatisch den ausgewählten Adressbüchern hinzufügt.">
<!ENTITY emailsCollectionEnabledLabel "Aktiviert">
<!ENTITY emailsCollectionMailNameLabel "Email-Konto">
<!ENTITY emailsCollectionABNameLabel "Adressbuch">
<!ENTITY emailsCollectionCatNameLabel "Kategorie">
<!ENTITY addEmailsCollectionLabel "Hinzufügen">
<!ENTITY addEmailsCollectionAccesskey "H">
<!ENTITY renameEmailsCollectionLabel "Bearbeiten">
<!ENTITY renameEmailsCollectionAccesskey "B">
<!ENTITY deleteEmailsCollectionLabel "Löschen">
<!ENTITY deleteEmailsCollectionAccesskey "L">
<!ENTITY logGroupboxLabel "Log">
<!ENTITY debugModeLabel "Debug-Modus">
<!ENTITY debugModeAccesskey "D">
<!ENTITY debugModeTooltip "Im Debug-Modus werden mehr Log-Ausgaben erzeugt.">
<!ENTITY statusInformationLineNumberLabel "Zeilenanzahl der Logdatei:">
<!ENTITY statusInformationLineNumberAccesskey "Z">

<!ENTITY tabsEditingGroupboxLabel "Anzeige der Tabs">
<!ENTITY noteTabLabel "Notizen">
<!ENTITY noteTabAccesskey "N">
<!ENTITY listTabLabel "Liste">
<!ENTITY listTabAccesskey "L">
<!ENTITY mailPopularityTabLabel "Email Popularität">
<!ENTITY mailPopularityTabAccesskey "P">
<!ENTITY technicalTabLabel "Technische Details">
<!ENTITY technicalTabAccesskey "T">
<!ENTITY vCardTabLabel "vCard">
<!ENTITY vCardTabAccesskey "V">
<!ENTITY advancedTab1Label "Erweitert">
<!ENTITY localizeEditingGroupboxLabel "Lokalisieren">
<!ENTITY localizeEngineOpenStreetMapLabel "OpenStreetMap">
<!ENTITY localizeEngineOpenStreetMapAccesskey "O">
<!ENTITY localizeEngineGoogleMapsLabel "Google Maps">
<!ENTITY localizeEngineGoogleMapsAccesskey "G">
<!ENTITY localizeEngineBingMapsLabel "Bing Maps">
<!ENTITY localizeEngineBingMapsAccesskey "B">
<!ENTITY localizeTargetLabel "Externe Links öffnen">
<!ENTITY localizeTargetInLabel "Innerhalb Thunderbirds">
<!ENTITY localizeTargetInAccesskey "I">
<!ENTITY localizeTargetOutLabel "Außerhalb Thunderbirds">
<!ENTITY localizeTargetOutAccesskey "A">
<!ENTITY showNameAsLabel "Namen anzeigen">
<!ENTITY showNameAsLFLabel "Nachname Vorname">
<!ENTITY showNameAsLFAccesskey "c">
<!ENTITY showNameAsLFCommaLabel "Nachname, Vorname">
<!ENTITY showNameAsLFCommaAccesskey "h">
<!ENTITY showNameAsFLLabel "Vorname Nachname">
<!ENTITY showNameAsFLAccesskey "m">
<!ENTITY showNameAsDSPLabel "Anzeigename">
<!ENTITY showNameAsDSPAccesskey "A">
<!ENTITY adrFormulaCaption "Adresse">
<!ENTITY adrFormulaLabel "Formel für das Format der Adressen:">
<!ENTITY adrFormulaAccesskey "F">
<!ENTITY resetAdrFormulaLabel "Zurücksetzen auf Standard">
<!ENTITY resetAdrFormulaAccesskey "Z">
<!ENTITY miscEditingGroupboxLabel "Sonstiges">
<!ENTITY preferEmailEditionLabel "Durch einen Doppelklick den Kontakt bearbeiten anstatt eine E-Mail zu verfassen">
<!ENTITY preferEmailEditionAccesskey "D">
<!ENTITY dateDisplayedFormatLabel "Datums-Textformat:">
<!ENTITY dateDisplayedFormatAccesskey "x">
<!ENTITY defaultRegionLabel "Standardregion:">
<!ENTITY defaultRegionAccesskey "S">
<!ENTITY defaultRegionTooltip "Diese Region wird als Standardregion für Adressen sowie als Format für Telefonnummern verwendet.">
<!ENTITY preferenceGroupboxLabel "Feld PREF">
<!ENTITY usePreferenceValueLabel "Benutze einen Präferenzwert (vCard 4.0)">
<!ENTITY usePreferenceValueAccesskey "P">
<!ENTITY preferenceValueLabel "Label des Werts des Felds PREF">
<!ENTITY preferenceValueAccesskey "W">

<!ENTITY editionGroupboxLabel "Auswahl der Felder">
<!ENTITY editionWarn "Bei der Bearbeitung eines Kontakts wird CardBook defaultmäßig nur die ausgewählten Felder anzeigen.">
<!ENTITY selectAllFieldsLabel "Alles auswählen">

<!ENTITY ABtypesGroupboxLabel "Adressbuch">
<!ENTITY typesCategoryGoogleLabel "Google">
<!ENTITY typesCategoryGoogleAccesskey "G">
<!ENTITY typesCategoryAppleLabel "Apple">
<!ENTITY typesCategoryAppleAccesskey "A">
<!ENTITY typesCategoryYahooLabel "Yahoo!">
<!ENTITY typesCategoryYahooAccesskey "Y">
<!ENTITY typesCategoryOthersLabel "Anderes">
<!ENTITY typesCategoryOthersAccesskey "n">
<!ENTITY typesGroupboxLabel "Typen">
<!ENTITY typesCategoryAdrLabel "Adresse">
<!ENTITY typesCategoryAdrAccesskey "d">
<!ENTITY typesCategoryEmailLabel "Email">
<!ENTITY typesCategoryEmailAccesskey "E">
<!ENTITY typesCategoryImppLabel "Instant Messenger">
<!ENTITY typesCategoryImppAccesskey "I">
<!ENTITY typesCategoryTelLabel "Telefon">
<!ENTITY typesCategoryTelAccesskey "T">
<!ENTITY typesCategoryUrlLabel "URL">
<!ENTITY typesCategoryUrlAccesskey "R">
<!ENTITY typesLabelLabel "Label">
<!ENTITY addTypeLabel "Hinzufügen">
<!ENTITY addTypeAccesskey "H">
<!ENTITY renameTypeLabel "Bearbeiten">
<!ENTITY renameTypeAccesskey "B">
<!ENTITY deleteTypeLabel "Löschen">
<!ENTITY deleteTypeAccesskey "L">
<!ENTITY resetTypeLabel "Zurücksetzen auf Standard">
<!ENTITY resetTypeAccesskey "Z">

<!ENTITY IMPPGroupboxWarn "Diese Einstellungen ermöglichen das direkte Öffnen einer IM Adresse mit dem angegebenen Protokoll.">
<!ENTITY IMPPGroupboxLabel "IM Adressen">
<!ENTITY preferIMPPPrefLabel "Bevorzuge IM Adressen markiert mit Stern ">
<!ENTITY preferIMPPPrefAccesskey "I">
<!ENTITY preferIMPPPrefWarn "Ist keine IM Adresse markiert mit Stern, so werden alle IM Adressen vorgeschlagen.">
<!ENTITY IMPPCodeLabel "Code">
<!ENTITY IMPPLabelLabel "Label">
<!ENTITY IMPPProtocolLabel "Protokoll">
<!ENTITY addIMPPLabel "Hinzufügen">
<!ENTITY addIMPPAccesskey "H">
<!ENTITY renameIMPPLabel "Bearbeiten">
<!ENTITY renameIMPPAccesskey "B">
<!ENTITY deleteIMPPLabel "Löschen">
<!ENTITY deleteIMPPAccesskey "L">
<!ENTITY URLPhoneGroupboxLabel "URL zum Öffnen von Softphones">
<!ENTITY URLPhoneGroupboxWarn "Dieser Abschnitt trifft nur zu, wenn man ein url Protokoll für das Telefon festgelegt hat. Die URL sollte $1 als Platzhalter für die Telefonnummer enthalten.">
<!ENTITY URLPhoneURLLabel "URL:">
<!ENTITY URLPhoneURLAccesskey "U">
<!ENTITY URLPhoneUserLabel "Benutzername:">
<!ENTITY URLPhoneUserAccesskey "z">
<!ENTITY URLPhonePasswordLabel "Passwort:">
<!ENTITY URLPhonePasswordAccesskey "P">
<!ENTITY URLPhonePasswordShowLabel "Passwort anzeigen">
<!ENTITY URLPhonePasswordShowAccesskey "a">
<!ENTITY URLPhoneBackgroundLabel "Im Hintergrund ausführen">
<!ENTITY URLPhoneBackgroundAccesskey "S">
<!ENTITY encryptionGroupboxLabel "Verschlüsselung">
<!ENTITY encryptionExplanation "CardBook kann Ihre lokalen Daten verschlüsseln. Der dafür benötigte geheime Schlüssel wird im Passwort-Manager gespeichert. Wenn Sie diesen Schlüssel verlieren, ist die Wiederherstellung Ihrer lokalen Daten unmöglich. Für einen vollständigen Schutz empfiehlt es sich die Thunderbird Master-Passwort Funktion zu aktivieren.">
<!ENTITY localDataEncryptionEnabledLabel "Lokal zwischengespeichterte Adresskarten verschlüsseln">
<!ENTITY localDataEncryptionEnabledAccesskey "v">

<!ENTITY customFieldRankLabel "Rang">
<!ENTITY customFieldCodeLabel "Feldname">
<!ENTITY customFieldLabelLabel "Feldlabel">
<!ENTITY customFieldsPersTitleLabel "Persönliche Informationen">
<!ENTITY customFieldsPersTitleAccesskey "P">
<!ENTITY customFieldsOrgTitleLabel "Berufliche Informationen">
<!ENTITY customFieldsOrgTitleAccesskey "B">
<!ENTITY addCustomFieldsLabel "Hinzufügen">
<!ENTITY addCustomFieldsAccesskey "H">
<!ENTITY renameCustomFieldsLabel "Bearbeiten">
<!ENTITY renameCustomFieldsAccesskey "B">
<!ENTITY deleteCustomFieldsLabel "Löschen">
<!ENTITY deleteCustomFieldsAccesskey "L">

<!ENTITY orgWarn "Das Feld ORG darf mehrere durch SEMIKOLON getrennte Zeichenketten enthalten, z.B.: yourcompany;yourservice;yourdepartment. Die Felder unten definieren die Namen der jeweiligen Zeichenketten.">
<!ENTITY orgRankLabel "Rang">
<!ENTITY orgLabelLabel "Feldlabel">

<!ENTITY listPanelLabel "Falls Ihr Server keine Kategorien unterstützt (z.B. Google, Apple, Zimbra, etc.), dann sind Listen eine gute Alternative, um Emails an Gruppen zu senden. vCard 3.0 hat keine native Unterstützung für Listen; stattdessen können Benutzerdefinierte Felder verwendet werden. Wenn Sie Listen in vCard 3.0 benutzen möchten, dann müssen Sie diese Felder definieren.">
<!ENTITY listGroupboxLabel "Benutzerdefinierte Felder für Listen">
<!ENTITY kindCustomLabel "Typ:">
<!ENTITY kindCustomAccesskey "T">
<!ENTITY memberCustomLabel "Mitglied:">
<!ENTITY memberCustomAccesskey "M">
<!ENTITY resetListLabel "Zurücksetzen auf Standard">
<!ENTITY resetListAccesskey "Z">

<!ENTITY syncGroupboxLabel "Synchronisieren">
<!ENTITY syncWarn "Änderungen können nur durch eine Synchronisation auf den Server übertragen werden.">
<!ENTITY syncAfterChangeLabel "Synchronisieren nach Änderungen">
<!ENTITY syncAfterChangeAccesskey "c">
<!ENTITY initialSyncLabel "Initiale Synchronisation beim Start von Thunderbird">
<!ENTITY initialSyncAccesskey "I">
<!ENTITY initialSyncDelayLabel "Verzögerung für erste Server-Anfrage (Sekunden):">
<!ENTITY initialSyncDelayAccesskey "V">
<!ENTITY solveConflictsLabel "Falls ein Kontakt sowohl lokal als auch serverseitig geändert worden ist:">
<!ENTITY solveConflictsLocalLabel "Bevorzuge die lokale Änderung">
<!ENTITY solveConflictsLocalAccesskey "L">
<!ENTITY solveConflictsRemoteLabel "Bevorzuge die serverseitige Änderung">
<!ENTITY solveConflictsRemoteAccesskey "S">
<!ENTITY solveConflictsUserLabel "Frage den Benutzer">
<!ENTITY solveConflictsUserAccesskey "B">
<!ENTITY maxModifsPushedLabel "Maximale Anzahl an Änderungen, die pro Synchronisation angestoßen werden:">
<!ENTITY maxModifsPushedAccesskey "M">
<!ENTITY requestsGroupboxLabel "Anfragen">
<!ENTITY requestsTimeoutLabel "Timeout (Sekunden):">
<!ENTITY requestsTimeoutAccesskey "T">
<!ENTITY multigetLabel "Bei GET-Anfragen Karten gruppieren nach:">
<!ENTITY multigetAccesskey "G">
<!ENTITY discoveryGroupboxLabel "Adressbücher Erkennung">
<!ENTITY discoveryWarn "Bei jeder Synchronisierung wird eine Überprüfung der unten aufgeführten URLs durchgeführt, um nach neuen oder gelöschten Adressbüchern zu suchen.">
<!ENTITY discoveryAccountsGroupboxLabel "Konten">
<!ENTITY advancedSyncGroupboxLabel "Erweiterte Eigenschaften">
<!ENTITY decodeReportLabel "Dekodiere URLs für REPORT Anfragen">
<!ENTITY decodeReportAccesskey "D">

<!ENTITY miscEmailGroupboxLabel "Email-Versand">
<!ENTITY preferEmailPrefLabel "Adressen mit dem Typ „pref” bevorzugen">
<!ENTITY preferEmailPrefAccesskey "P">
<!ENTITY preferEmailPrefWarn "Ist keine Adresse mit dem Typ „pref” vorhanden, so werden alle Adressen genutzt.">
<!ENTITY warnEmptyEmailsLabel "Mich warnen, wenn ich versuche an einen Kontakt zu senden, zu dem keine E-Mailadresse gespeichert ist">
<!ENTITY warnEmptyEmailsAccesskey "W">
<!ENTITY useOnlyEmailLabel "Nur die E-Mail-Adressen verwenden">
<!ENTITY useOnlyEmailAccesskey "U">
<!ENTITY attachVCardWarn "Automatisch vCard an Emails anhängen: Eine vCard pro E-Mail-Konto auswählen. Falls aktiviert wird diese vCard an jede ausgehende E-Mail angehängt.">
<!ENTITY attachVCardGroupboxLabel "Fürs an Email anhängen">
<!ENTITY accountsVCardsEnabledLabel "Aktiviert">
<!ENTITY accountsVCardsMailLabel "Email-Konto">
<!ENTITY accountsVCardsABNameLabel "Adressbuch">
<!ENTITY accountsVCardsFileNameLabel "Anhang">
<!ENTITY accountsVCardsFnLabel "Kontakt">
<!ENTITY addVCardLabel "Hinzufügen">
<!ENTITY addVCardAccesskey "H">
<!ENTITY renameVCardLabel "Bearbeiten">
<!ENTITY renameVCardAccesskey "B">
<!ENTITY deleteVCardLabel "Löschen">
<!ENTITY deleteVCardAccesskey "L">

<!ENTITY birthdayListWarn "Geburtstage werden nur in Adressbüchern von CardBook gesucht.">
<!ENTITY addressbooksGroupboxLabel "Adressbücher">
<!ENTITY selectAllABsLabel "Alles auswählen">
<!ENTITY searchForLabel "Suche nach:">
<!ENTITY bdayLabel "Geburtstag">
<!ENTITY bdayAccesskey "G">
<!ENTITY anniversaryLabel "Jahrestag (nur in vCard 4.0)">
<!ENTITY anniversaryAccesskey "J">
<!ENTITY deathdateLabel "Todestag (nur in vCard 4.0)">
<!ENTITY deathdateAccesskey "T">
<!ENTITY eventsLabel "Termine">
<!ENTITY eventsAccesskey "e">
<!ENTITY calendarsGroupboxLabel "Kalender">
<!ENTITY selectAllCalendarsLabel "Alles auswählen">

<!ENTITY remindersTabLabel "Erinnerungen">
<!ENTITY remindViaPopupWindowGroupboxLabel "Erinnerung mit Popup-Fenster">
<!ENTITY numberOfDaysForSearchingLabel "Anzahl der Tage zum Suchen der Geburtstage:">
<!ENTITY numberOfDaysForSearchingAccesskey "A">
<!ENTITY showPopupOnStartupLabel "Zeige ein Popup-Fenster beim Starten">
<!ENTITY showPopupOnStartupAccesskey "S">
<!ENTITY showPeriodicPopupLabel "Zeige ein Popup-Fenster täglich">
<!ENTITY showPeriodicPopupAccesskey "t">
<!ENTITY periodicPopupTimeLabel "Zeit (hh:mm):">
<!ENTITY periodicPopupTimeAccesskey "Z">
<!ENTITY showPopupEvenIfNoBirthdayLabel "Zeige ein Popup-Fenster, falls die Liste der Geburtstage leer ist">
<!ENTITY showPopupEvenIfNoBirthdayAccesskey "f">
<!ENTITY remindViaLightningGroupboxLabel "Erinnerung mit Lightning (Erweiterung für Thunderbird)">
<!ENTITY numberOfDaysForWritingLabel "Anzahl der Tage zum Eintragen der Geburtstage:">
<!ENTITY numberOfDaysForWritingAccesskey "E">
<!ENTITY syncWithLightningOnStartupLabel "Synchronisiere mit Lightning beim Starten">
<!ENTITY syncWithLightningOnStartupAccesskey "L">
<!ENTITY calendarEntryTitleLabel "Titel für Termin:">
<!ENTITY calendarEntryTitleTooltip "Das Feld sollte zwei Platzhalter &#37;S für Namen und Alter enthalten. Falls leer, wird eine Standard-Nachricht verwendet.">
<!ENTITY calendarEntryTitleAccesskey "T">
<!ENTITY resetCalendarEntryTitleButtonLabel "Zurücksetzen auf Standard">
<!ENTITY resetCalendarEntryTitleButtonAccesskey "Z">
<!ENTITY calendarEntryTimeLabel "Zeit für Termin (hh:mm):">
<!ENTITY calendarEntryTimeAccesskey "r">
<!ENTITY calendarEntryWholeDayLabel "Ganztägiger Termin">
<!ENTITY calendarEntryWholeDayAccesskey "G">
<!ENTITY calendarEntryAlarmLabel "Erinnerung(en) n Stunden (Beispiel: 4,+1):">
<!ENTITY calendarEntryAlarmTooltip "Wieviele Stunden vor dem Geburtstag soll die Erinnerung angezeigt werden. Falls leer, keine Erinnerung. Falls Sie mehrere Erinnerungen wünschen, geben Sie diese durch Kommas getrennt ein.">
<!ENTITY calendarEntryAlarmAccesskey "E">
<!ENTITY calendarEntryCategoriesLabel "Kategorie(n) für Termin:">
<!ENTITY calendarEntryCategoriesTooltip "Eine Kategorie bzw. mehrere durch Komma getrennte Kategorien. Falls leer, keine Kategorie.">
<!ENTITY calendarEntryCategoriesAccesskey "K">
