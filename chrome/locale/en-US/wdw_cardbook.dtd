<!ENTITY wdw_cardbookWindowLabel "CardBook">

<!ENTITY enableAccountsTooltip "Enable or disable search and synchronization of address books.">
<!ENTITY colorAccountsTooltip "Color address books search results (use when searches span multiple address books).">
<!ENTITY readonlyAccountsTooltip "Set address books in read-only or read-write mode.">
<!ENTITY searchRemoteLabel "This address book shows contacts only after a search">

<!ENTITY cardbookAccountMenuLabel "Address Book">

<!ENTITY cardbookAccountMenuAddServerLabel "New Address Book">
<!ENTITY cardbookAccountMenuEditServerLabel "Edit Address Book">
<!ENTITY cardbookAccountMenuCloseServerLabel "Delete Address Book">
<!ENTITY cardbookAccountMenuSyncLabel "Synchronize Address Book">
<!ENTITY cardbookAccountMenuSyncsLabel "Synchronize All Address Books">

<!ENTITY cardbookContactsMenuLabel "Contacts">

<!ENTITY cardbookToolsMenuLabel "Tools">
<!ENTITY cardbookToolsMenuBirthdayListLabel "View Anniversary List">
<!ENTITY cardbookToolsMenuSyncLightningLabel "Add Anniversaries to Calendar">
<!ENTITY cardbookToolsMenuFindAllDuplicatesLabel "Find Duplicate Contacts in all Address Books">
<!ENTITY cardbookToolsMenuFindSingleDuplicatesLabel "Find Duplicate Contacts in Current Address Book">
<!ENTITY cardbookToolsMenuPrefsLabel "CardBook Preferences">

<!ENTITY cardbookToolbarLabel "CardBook Toolbar">
<!ENTITY cardbookToolbarAccesskey "a">
<!ENTITY cardbookABPaneToolbarLabel "CardBook Address Book Pane Toolbar">
<!ENTITY cardbookABPaneToolbarAccesskey "d">
<!ENTITY CustomizeCardBookToolbarLabel "Customize…">
<!ENTITY CustomizeCardBookToolbarAccesskey "C">
<!ENTITY cardbookToolbarAddServerButtonLabel "New Address Book">
<!ENTITY cardbookToolbarAddServerButtonTooltip "Add a remote or local address book">
<!ENTITY cardbookToolbarSyncButtonLabel "Synchronize">
<!ENTITY cardbookToolbarSyncButtonTooltip "Synchronize all remote address books">
<!ENTITY cardbookToolbarWriteButtonLabel "Write">
<!ENTITY cardbookToolbarWriteButtonTooltip "Create a new message">
<!ENTITY cardbookToolbarChatButtonLabel "Connect To">
<!ENTITY cardbookToolbarChatButtonTooltip "Send an instant message or chat">
<!ENTITY cardbookToolbarConfigurationButtonLabel "Preferences">
<!ENTITY cardbookToolbarConfigurationButtonTooltip "CardBook Preferences">
<!ENTITY cardbookToolbarAddContactButtonLabel "New Contact">
<!ENTITY cardbookToolbarAddContactButtonTooltip "Create a new address book contact">
<!ENTITY cardbookToolbarAddListButtonLabel "New List">
<!ENTITY cardbookToolbarAddListButtonTooltip "New Mailing List">
<!ENTITY cardbookToolbarEditButtonLabel "Edit">
<!ENTITY cardbookToolbarEditButtonTooltip "Edit the selected contact">
<!ENTITY cardbookToolbarRemoveButtonLabel "Delete">
<!ENTITY cardbookToolbarRemoveButtonTooltip "Delete selected contact">
<!ENTITY cardbookToolbarPrintButtonLabel "Print Preview">
<!ENTITY cardbookToolbarPrintButtonTooltip "Print preview of the selection">
<!ENTITY cardbookToolbarBackButtonLabel "Undo">
<!ENTITY cardbookToolbarBackButtonTooltip "Go back one action">
<!ENTITY cardbookToolbarForwardButtonLabel "Redo">
<!ENTITY cardbookToolbarForwardButtonTooltip "Go forward one action">
<!ENTITY cardbookToolbarAppMenuButtonLabel "CardBook menu">
<!ENTITY cardbookToolbarAppMenuButtonTooltip "CardBook menu">
<!ENTITY cardbookToolbarSearchBoxLabel "Contacts search">
<!ENTITY cardbookToolbarSearchBoxTooltip "Contacts search">
<!ENTITY cardbookToolbarComplexSearchLabel "Saved Search">
<!ENTITY cardbookToolbarComplexSearchTooltip "Create a new saved search folder">
<!ENTITY cardbookToolbarThMenuButtonLabel "AppMenu">
<!ENTITY cardbookToolbarThMenuButtonTooltip "Display the &brandShortName; Menu">
<!ENTITY cardbookToolbarABMenuLabel "Address Books View">
<!ENTITY cardbookToolbarABMenuTooltip "Filter how address books are displayed">

<!ENTITY generalTabLabel "General">
<!ENTITY mailPopularityTabLabel "Mail Popularity">
<!ENTITY technicalTabLabel "Technical">
<!ENTITY vCardTabLabel "vCard">

<!ENTITY categoriesGroupboxLabel "Categories">
<!ENTITY noteTabLabel "Notes">

<!ENTITY miscGroupboxLabel "Miscellaneous">
<!ENTITY othersGroupboxLabel "Other">
<!ENTITY techGroupboxLabel "Technical">
<!ENTITY labelGroupboxLabel "Labels">
<!ENTITY class1Label "Class">
<!ENTITY geoLabel "Geo">
<!ENTITY mailerLabel "Mailer">
<!ENTITY agentLabel "Agent">
<!ENTITY keyLabel "Key">
<!ENTITY photolocalURILabel "Local photo">
<!ENTITY logolocalURILabel "Local logo">
<!ENTITY soundlocalURILabel "Local sound">
<!ENTITY photoURILabel "Photo">
<!ENTITY logoURILabel "Logo">
<!ENTITY soundURILabel "Sound">
<!ENTITY prodidLabel "Prodid">
<!ENTITY sortstringLabel "Sort String">
<!ENTITY uidLabel "Contact id">
<!ENTITY versionLabel "Version">
<!ENTITY tzLabel "Time Zone">

<!ENTITY dirPrefIdLabel "Address Book id">
<!ENTITY cardurlLabel "Card URL">
<!ENTITY cacheuriLabel "Cache URI">
<!ENTITY revLabel "Last Updated">
<!ENTITY etagLabel "Etag">

<!ENTITY localizeadrTreeLabel "Show on Map">
<!ENTITY toEmailEmailTreeLabel "Write a New Message">
<!ENTITY ccEmailEmailTreeLabel "Write a New Message (copy / cc)">
<!ENTITY bccemailemailTreeLabel "Write a New Message (blind copy / bcc)">
<!ENTITY findemailemailTreeLabel "Find Emails Related to this Email Address">
<!ENTITY findeventemailTreeLabel "Find Calendar Events Related to this Email Address">
<!ENTITY openURLTreeLabel "Open URL">

<!ENTITY toEmailCardFromAccountsOrCatsLabel "Write a New Message">
<!ENTITY toEmailCardFromCardsLabel "Write a New Message">
<!ENTITY ccEmailCardFromAccountsOrCatsLabel "Write a New Message (copy / cc)">
<!ENTITY ccEmailCardFromCardsLabel "Write a New Message (copy / cc)">
<!ENTITY bccEmailCardFromAccountsOrCatsLabel "Write a New Message (blind copy / bcc)">
<!ENTITY bccEmailCardFromCardsLabel "Write a New Message (blind copy / bcc)">
<!ENTITY shareCardByEmailFromAccountsOrCatsLabel "Share by Email">
<!ENTITY shareCardByEmailFromCardsLabel "Share by Email">
<!ENTITY categoryLabel "Category">
<!ENTITY findEmailsFromCardsLabel "Find Emails Related to this Contact">
<!ENTITY findEventsFromCardsLabel "Find Calendar Events Related to this Contact">
<!ENTITY localizeCardFromCardsLabel "Show on Map">
<!ENTITY openURLCardFromCardsLabel "Open URLs">
<!ENTITY cutCardFromAccountsOrCatsLabel "Cut">
<!ENTITY cutCardFromCardsLabel "Cut">
<!ENTITY copyCardFromAccountsOrCatsLabel "Copy">
<!ENTITY copyCardFromCardsLabel "Copy">
<!ENTITY pasteCardFromAccountsOrCatsLabel "Paste">
<!ENTITY pasteCardFromCardsLabel "Paste">
<!ENTITY pasteEntryLabel "Paste Entry">
<!ENTITY exportCardToFileLabel "Export to a File">
<!ENTITY exportCardToDirLabel "Export to a Directory">
<!ENTITY importCardFromFileLabel "Import Contacts from a File">
<!ENTITY importCardFromDirLabel "Import Contacts from a Directory">
<!ENTITY findDuplicatesFromAccountsOrCatsLabel "Find Duplicate Contacts in Current Address Book">
<!ENTITY generateFnFromAccountsOrCatsLabel "Generate Display Names">
<!ENTITY mergeCardsFromCardsLabel "Merge Contacts">
<!ENTITY duplicateCardFromCardsLabel "Duplicate Contact">
<!ENTITY convertListToCategoryFromCardsLabel "Convert List to Category">
<!ENTITY editAccountFromAccountsOrCatsLabel "Edit Address Book">
<!ENTITY syncAccountFromAccountsOrCatsLabel "Synchronize Address Book">
<!ENTITY removeAccountFromAccountsOrCatsLabel "Delete Address Book">

<!ENTITY IMPPMenuLabel "Connect To">
