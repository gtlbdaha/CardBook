<!ENTITY wdw_cardbookWindowLabel "CardBook">

<!ENTITY enableAccountsTooltip "Включение и отключение поиска и синхронизации адресных книг.">
<!ENTITY colorAccountsTooltip "Выделять цветом адресные книги в результатах поиска (используется при сквозном поиске в нескольких адресных книгах).">
<!ENTITY readonlyAccountsTooltip "Выбор режим доступа к адресной книги: «только чтение» или «чтение и запись».">
<!ENTITY searchRemoteLabel "Это адресная книга покажет контакты только после поиска">

<!ENTITY cardbookAccountMenuLabel "Адресная книга">

<!ENTITY cardbookAccountMenuAddServerLabel "Добавить адресную книгу">
<!ENTITY cardbookAccountMenuEditServerLabel "Редактировать адресную книгу">
<!ENTITY cardbookAccountMenuCloseServerLabel "Удалить адресную книгу">
<!ENTITY cardbookAccountMenuSyncLabel "Синхронизировать адресную книгу">
<!ENTITY cardbookAccountMenuSyncsLabel "Синхронизировать все адресные книги">

<!ENTITY cardbookContactsMenuLabel "Контакты">

<!ENTITY cardbookToolsMenuLabel "Инструменты">
<!ENTITY cardbookToolsMenuBirthdayListLabel "Открыть список дней рождения">
<!ENTITY cardbookToolsMenuSyncLightningLabel "Добавить дни рождения в календарь">
<!ENTITY cardbookToolsMenuFindAllDuplicatesLabel "Искать повторяющиеся контакты во всех адресных книгах">
<!ENTITY cardbookToolsMenuFindSingleDuplicatesLabel "Искать повторяющиеся контакты во текущей адресной книге">
<!ENTITY cardbookToolsMenuPrefsLabel "Настроить CardBook…">

<!ENTITY cardbookToolbarLabel "Панель инструментов CardBook">
<!ENTITY cardbookToolbarAccesskey "П">
<!ENTITY cardbookABPaneToolbarLabel "Панель адресных книг CardBook">
<!ENTITY cardbookABPaneToolbarAccesskey "д">
<!ENTITY CustomizeCardBookToolbarLabel "Настроить…">
<!ENTITY CustomizeCardBookToolbarAccesskey "Н">
<!ENTITY cardbookToolbarAddServerButtonLabel "Добавить адресную книгу">
<!ENTITY cardbookToolbarAddServerButtonTooltip "Добавление адресной книги, расположенной локально или на сервере">
<!ENTITY cardbookToolbarSyncButtonLabel "Синхронизация">
<!ENTITY cardbookToolbarSyncButtonTooltip "Синхронизация всех адресных книг">
<!ENTITY cardbookToolbarWriteButtonLabel "Создать сообщение">
<!ENTITY cardbookToolbarWriteButtonTooltip "Создание сообщения">
<!ENTITY cardbookToolbarChatButtonLabel "Соединиться">
<!ENTITY cardbookToolbarChatButtonTooltip "Отправить мгновенное сообщение или начать беседу">
<!ENTITY cardbookToolbarConfigurationButtonLabel "Параметры">
<!ENTITY cardbookToolbarConfigurationButtonTooltip "Настроить параметры CardBook">
<!ENTITY cardbookToolbarAddContactButtonLabel "Создать контакт">
<!ENTITY cardbookToolbarAddContactButtonTooltip "Создание нового контакта адресной книги">
<!ENTITY cardbookToolbarAddListButtonLabel "Создать список">
<!ENTITY cardbookToolbarAddListButtonTooltip "Создание нового списка рассылки">
<!ENTITY cardbookToolbarEditButtonLabel "Редактировать">
<!ENTITY cardbookToolbarEditButtonTooltip "Редактирование существующего контакта">
<!ENTITY cardbookToolbarRemoveButtonLabel "Удалить">
<!ENTITY cardbookToolbarRemoveButtonTooltip "Удаление существующего контакта">
<!ENTITY cardbookToolbarPrintButtonLabel "Предварительный просмор печати">
<!ENTITY cardbookToolbarPrintButtonTooltip "Разрешить печатать выделение">
<!ENTITY cardbookToolbarBackButtonLabel "Отменить">
<!ENTITY cardbookToolbarBackButtonTooltip "Отменить последнее действие">
<!ENTITY cardbookToolbarForwardButtonLabel "Повторить">
<!ENTITY cardbookToolbarForwardButtonTooltip "Повторить последнее действие">
<!ENTITY cardbookToolbarAppMenuButtonLabel "Меню CardBook">
<!ENTITY cardbookToolbarAppMenuButtonTooltip "Меню CardBook">
<!ENTITY cardbookToolbarSearchBoxLabel "Поиск контактов">
<!ENTITY cardbookToolbarSearchBoxTooltip "Поиск контактов">
<!ENTITY cardbookToolbarComplexSearchLabel "Расширеный поиск">
<!ENTITY cardbookToolbarComplexSearchTooltip "Поиск с использованием нескольких условий">
<!ENTITY cardbookToolbarThMenuButtonLabel "Меню приложения">
<!ENTITY cardbookToolbarThMenuButtonTooltip "Показать меню &brandShortName;">
<!ENTITY cardbookToolbarABMenuLabel "Режим просмотра адресных книг">
<!ENTITY cardbookToolbarABMenuTooltip "Настройка режимов просмотра адресных книг">

<!ENTITY generalTabLabel "Основные">
<!ENTITY mailPopularityTabLabel "Рейтинг email">
<!ENTITY technicalTabLabel "Технические данные">
<!ENTITY vCardTabLabel "vCard">

<!ENTITY categoriesGroupboxLabel "Категории">
<!ENTITY noteTabLabel "Заметка">

<!ENTITY miscGroupboxLabel "Разное">
<!ENTITY othersGroupboxLabel "Другое">
<!ENTITY techGroupboxLabel "Тех.данные">
<!ENTITY labelGroupboxLabel "Метки">
<!ENTITY class1Label "Класс">
<!ENTITY geoLabel "Geo">
<!ENTITY mailerLabel "Почтовая программа">
<!ENTITY agentLabel "Агент">
<!ENTITY keyLabel "Ключ">
<!ENTITY photolocalURILabel "Локальное фото">
<!ENTITY logolocalURILabel "Локальный логотип">
<!ENTITY soundlocalURILabel "Локальный звук">
<!ENTITY photoURILabel "Фотография">
<!ENTITY logoURILabel "Логотип">
<!ENTITY soundURILabel "Звук">
<!ENTITY prodidLabel "Product ID">
<!ENTITY sortstringLabel "Строка для сортировки">
<!ENTITY uidLabel "UID">
<!ENTITY versionLabel "Версия">
<!ENTITY tzLabel "Часовой пояс">

<!ENTITY dirPrefIdLabel "DirPrefId">
<!ENTITY cardurlLabel "URL карточки">
<!ENTITY cacheuriLabel "URI в кеше">
<!ENTITY revLabel "Последнее обновление">
<!ENTITY etagLabel "Etag">

<!ENTITY localizeadrTreeLabel "Показать на карте">
<!ENTITY toEmailEmailTreeLabel "Создать сообщение">
<!ENTITY ccEmailEmailTreeLabel "Создать сообщение (копию)">
<!ENTITY bccemailemailTreeLabel "Создать сообщение (скрытую копию)">
<!ENTITY findemailemailTreeLabel "Искать сообщения, связанные с этим адресом">
<!ENTITY findeventemailTreeLabel "Искать события календаря, связанные с этим адресом">
<!ENTITY openURLTreeLabel "Открыть URL">

<!ENTITY toEmailCardFromAccountsOrCatsLabel "Создать сообщение">
<!ENTITY toEmailCardFromCardsLabel "Создать сообщение">
<!ENTITY ccEmailCardFromAccountsOrCatsLabel "Создать сообщение (копию)">
<!ENTITY ccEmailCardFromCardsLabel "Создать сообщение (копию)">
<!ENTITY bccEmailCardFromAccountsOrCatsLabel "Создать сообщение (скрытую копию)">
<!ENTITY bccEmailCardFromCardsLabel "Создать сообщение (скрытую копию)">
<!ENTITY shareCardByEmailFromAccountsOrCatsLabel "Поделиться по email">
<!ENTITY shareCardByEmailFromCardsLabel "Поделиться по email">
<!ENTITY categoryLabel "Категория">
<!ENTITY findEmailsFromCardsLabel "Искать сообщения, связанные с этим контактом">
<!ENTITY findEventsFromCardsLabel "Искать события календаря, связанные с этим контактом">
<!ENTITY localizeCardFromCardsLabel "Показать на карте">
<!ENTITY openURLCardFromCardsLabel "Открыть URL">
<!ENTITY cutCardFromAccountsOrCatsLabel "Вырезать">
<!ENTITY cutCardFromCardsLabel "Вырезать">
<!ENTITY copyCardFromAccountsOrCatsLabel "Копировать">
<!ENTITY copyCardFromCardsLabel "Копировать">
<!ENTITY pasteCardFromAccountsOrCatsLabel "Вставить">
<!ENTITY pasteCardFromCardsLabel "Вставить">
<!ENTITY pasteEntryLabel "Вставить значение">
<!ENTITY exportCardToFileLabel "Экспортировать в файл">
<!ENTITY exportCardToDirLabel "Экспортировать в каталог">
<!ENTITY importCardFromFileLabel "Импортировать контакт из файла">
<!ENTITY importCardFromDirLabel "Импортировать контакт из каталога">
<!ENTITY findDuplicatesFromAccountsOrCatsLabel "Искать повторяющиеся контакты во текущей адресной книге">
<!ENTITY generateFnFromAccountsOrCatsLabel "Формировать поле «Отображаемое имя»">
<!ENTITY mergeCardsFromCardsLabel "Объединить контакты">
<!ENTITY duplicateCardFromCardsLabel "Скопировать контакт">
<!ENTITY convertListToCategoryFromCardsLabel "Преобразовать список в категорию">
<!ENTITY editAccountFromAccountsOrCatsLabel "Редактировать адресную книгу">
<!ENTITY syncAccountFromAccountsOrCatsLabel "Синхронизировать адресную книку">
<!ENTITY removeAccountFromAccountsOrCatsLabel "Удалить адресную книгу">

<!ENTITY IMPPMenuLabel "Соедениться с">
