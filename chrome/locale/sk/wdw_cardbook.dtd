<!ENTITY wdw_cardbookWindowLabel "CardBook">

<!ENTITY enableAccountsTooltip "Povoliť alebo zakázať vyhľadávanie a synchronizáciu adresárov.">
<!ENTITY colorAccountsTooltip "Farebné výsledky hľadania v adresároch (použite pri vyhľadávaní v niekoľkých adresároch).">
<!ENTITY readonlyAccountsTooltip "Nastaviť adresáre do režimu iba na čítanie alebo na čítanie aj zápis.">
<!ENTITY searchRemoteLabel "Tento adresár zobrazuje kontakty až po vyhľadaní">

<!ENTITY cardbookAccountMenuLabel "Adresár">

<!ENTITY cardbookAccountMenuAddServerLabel "Nový adresár">
<!ENTITY cardbookAccountMenuEditServerLabel "Upraviť adresár">
<!ENTITY cardbookAccountMenuCloseServerLabel "Vymazať adresár">
<!ENTITY cardbookAccountMenuSyncLabel "Synchronizovať adresár">
<!ENTITY cardbookAccountMenuSyncsLabel "Synchronizovať všetky adresáre">

<!ENTITY cardbookContactsMenuLabel "Kontakty">

<!ENTITY cardbookToolsMenuLabel "Nástroje">
<!ENTITY cardbookToolsMenuBirthdayListLabel "Zobraziť zoznam výročí">
<!ENTITY cardbookToolsMenuSyncLightningLabel "Pridať výročia do kalendára">
<!ENTITY cardbookToolsMenuFindAllDuplicatesLabel "Nájsť zdvojené kontakty vo všetkých adresároch">
<!ENTITY cardbookToolsMenuFindSingleDuplicatesLabel "Nájsť zdvojené kontakty v aktuálnom adresári">
<!ENTITY cardbookToolsMenuPrefsLabel "Nastavenia CardBook">

<!ENTITY cardbookToolbarLabel "Panel nástrojov CardBook">
<!ENTITY cardbookToolbarAccesskey "a">
<!ENTITY cardbookABPaneToolbarLabel "Panel nástrojov adresára CardBook">
<!ENTITY cardbookABPaneToolbarAccesskey "d">
<!ENTITY CustomizeCardBookToolbarLabel "Prispôsobiť…">
<!ENTITY CustomizeCardBookToolbarAccesskey "P">
<!ENTITY cardbookToolbarAddServerButtonLabel "Nový adresár">
<!ENTITY cardbookToolbarAddServerButtonTooltip "Pridať vzdialený alebo miestny adresár">
<!ENTITY cardbookToolbarSyncButtonLabel "Synchronizovať">
<!ENTITY cardbookToolbarSyncButtonTooltip "Synchronizovať všetky vzdialené adresáre">
<!ENTITY cardbookToolbarWriteButtonLabel "Napísať">
<!ENTITY cardbookToolbarWriteButtonTooltip "Vytvoriť novú správu">
<!ENTITY cardbookToolbarChatButtonLabel "Pripojiť">
<!ENTITY cardbookToolbarChatButtonTooltip "Poslať instantnú správu alebo chat">
<!ENTITY cardbookToolbarConfigurationButtonLabel "Nastavenia">
<!ENTITY cardbookToolbarConfigurationButtonTooltip "Nastavenia CardBook">
<!ENTITY cardbookToolbarAddContactButtonLabel "Nový kontakt">
<!ENTITY cardbookToolbarAddContactButtonTooltip "Vytvoriť nový kontakt v adresári">
<!ENTITY cardbookToolbarAddListButtonLabel "Nový zoznam">
<!ENTITY cardbookToolbarAddListButtonTooltip "Nový zoznam">
<!ENTITY cardbookToolbarEditButtonLabel "Upraviť">
<!ENTITY cardbookToolbarEditButtonTooltip "Upraviť vybratý kontakt">
<!ENTITY cardbookToolbarRemoveButtonLabel "Vymazať">
<!ENTITY cardbookToolbarRemoveButtonTooltip "Vymazať vybratý kontakt">
<!ENTITY cardbookToolbarPrintButtonLabel "Náhľad tlače">
<!ENTITY cardbookToolbarPrintButtonTooltip "Náhľad výberu pred tlačou">
<!ENTITY cardbookToolbarBackButtonLabel "Späť">
<!ENTITY cardbookToolbarBackButtonTooltip "Ísť späť o jeden krok">
<!ENTITY cardbookToolbarForwardButtonLabel "Znova">
<!ENTITY cardbookToolbarForwardButtonTooltip "Ísť dopredu o jeden krok">
<!ENTITY cardbookToolbarAppMenuButtonLabel "Ponuka CardBook">
<!ENTITY cardbookToolbarAppMenuButtonTooltip "Ponuka CardBook">
<!ENTITY cardbookToolbarSearchBoxLabel "Hľadanie v kontaktoch">
<!ENTITY cardbookToolbarSearchBoxTooltip "Hľadanie v kontaktoch">
<!ENTITY cardbookToolbarComplexSearchLabel "Uložené hľadania">
<!ENTITY cardbookToolbarComplexSearchTooltip "Vytvoriť nový priečinok uloženého vyhľadávania">
<!ENTITY cardbookToolbarThMenuButtonLabel "Ponuka aplikácie">
<!ENTITY cardbookToolbarThMenuButtonTooltip "Zobraziť ponuku &brandShortName;">
<!ENTITY cardbookToolbarABMenuLabel "Zobrazenie adresára">
<!ENTITY cardbookToolbarABMenuTooltip "Filtrovať spôsob zobrazenia adresárov">

<!ENTITY generalTabLabel "Všeobecné">
<!ENTITY mailPopularityTabLabel "Popularita emailov">
<!ENTITY technicalTabLabel "Technické">
<!ENTITY vCardTabLabel "vCard">

<!ENTITY categoriesGroupboxLabel "Kategórie">
<!ENTITY noteTabLabel "Poznámky">

<!ENTITY miscGroupboxLabel "Rôzne">
<!ENTITY othersGroupboxLabel "Iné">
<!ENTITY techGroupboxLabel "Technické">
<!ENTITY labelGroupboxLabel "Menovky">
<!ENTITY class1Label "Trieda">
<!ENTITY geoLabel "Geo">
<!ENTITY mailerLabel "Mailer">
<!ENTITY agentLabel "Agent">
<!ENTITY keyLabel "Kľúč">
<!ENTITY photolocalURILabel "Lokálna fotka">
<!ENTITY logolocalURILabel "Miestne logo">
<!ENTITY soundlocalURILabel "Miestny zvuk">
<!ENTITY photoURILabel "Fotka">
<!ENTITY logoURILabel "Logo">
<!ENTITY soundURILabel "Zvuk">
<!ENTITY prodidLabel "ID produktu">
<!ENTITY sortstringLabel "Reťazec zoradenia">
<!ENTITY uidLabel "ID kontaktu">
<!ENTITY versionLabel "Verzia">
<!ENTITY tzLabel "Časové pásmo">

<!ENTITY dirPrefIdLabel "ID Adresára">
<!ENTITY cardurlLabel "URL karty">
<!ENTITY cacheuriLabel "Uložiť URI do medzi-pamäte">
<!ENTITY revLabel "Naposledy aktualizované">
<!ENTITY etagLabel "Etag">

<!ENTITY localizeadrTreeLabel "Zobraziť na mape">
<!ENTITY toEmailEmailTreeLabel "Napísať novú správu">
<!ENTITY ccEmailEmailTreeLabel "Napísať novú správu (kópia - cc)">
<!ENTITY bccemailemailTreeLabel "Napísať novú správu (skrytá kópia - bcc)">
<!ENTITY findemailemailTreeLabel "Nájsť emaily súvisiace s touto emailovou adresou">
<!ENTITY findeventemailTreeLabel "Nájsť udalosti kalendára súvisiace s touto emailovou adresou">
<!ENTITY openURLTreeLabel "Otvoriť URL adresu">

<!ENTITY toEmailCardFromAccountsOrCatsLabel "Napísať novú správu">
<!ENTITY toEmailCardFromCardsLabel "Napísať novú správu">
<!ENTITY ccEmailCardFromAccountsOrCatsLabel "Napísať novú správu (kópia - cc)">
<!ENTITY ccEmailCardFromCardsLabel "Napísať novú správu (kópia - cc)">
<!ENTITY bccEmailCardFromAccountsOrCatsLabel "Napísať novú správu (skrytá kópia - bcc)">
<!ENTITY bccEmailCardFromCardsLabel "Napísať novú správu (skrytá kópia - bcc)">
<!ENTITY shareCardByEmailFromAccountsOrCatsLabel "Zdieľať cez email">
<!ENTITY shareCardByEmailFromCardsLabel "Zdieľať cez email">
<!ENTITY categoryLabel "Kategória">
<!ENTITY findEmailsFromCardsLabel "Nájsť emaily súvisiace s týmto kontaktom">
<!ENTITY findEventsFromCardsLabel "Nájsť udalosti kalendára súvisiace s týmto kontaktom">
<!ENTITY localizeCardFromCardsLabel "Zobraziť na mape">
<!ENTITY openURLCardFromCardsLabel "Otvoriť URL adresy">
<!ENTITY cutCardFromAccountsOrCatsLabel "Vystrihnúť">
<!ENTITY cutCardFromCardsLabel "Vystrihnúť">
<!ENTITY copyCardFromAccountsOrCatsLabel "Kopírovať">
<!ENTITY copyCardFromCardsLabel "Kopírovať">
<!ENTITY pasteCardFromAccountsOrCatsLabel "Vložiť">
<!ENTITY pasteCardFromCardsLabel "Vložiť">
<!ENTITY pasteEntryLabel "Vložiť záznam">
<!ENTITY exportCardToFileLabel "Exportovať do súboru">
<!ENTITY exportCardToDirLabel "Exportovať do priečinka">
<!ENTITY importCardFromFileLabel "Importovať kontakty zo súboru">
<!ENTITY importCardFromDirLabel "Importovať kontakty z priečinka">
<!ENTITY findDuplicatesFromAccountsOrCatsLabel "Nájsť zdvojené kontakty v aktuálnom adresári">
<!ENTITY generateFnFromAccountsOrCatsLabel "Vygenerovať zobrazované mená">
<!ENTITY mergeCardsFromCardsLabel "Zlúčiť kontakty">
<!ENTITY duplicateCardFromCardsLabel "Duplikovať kontakt">
<!ENTITY convertListToCategoryFromCardsLabel "Previesť zoznam na kategóriu">
<!ENTITY editAccountFromAccountsOrCatsLabel "Upraviť adresár">
<!ENTITY syncAccountFromAccountsOrCatsLabel "Synchronizovať adresár">
<!ENTITY removeAccountFromAccountsOrCatsLabel "Vymazať adresár">

<!ENTITY IMPPMenuLabel "Pripojiť">
